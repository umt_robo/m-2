/*
 Fading

 This example shows how to fade an LED using the analogWrite() function.

 The circuit:
 * LED attached from digital pin 9 to ground.

 Created 1 Nov 2008
 By David A. Mellis
 modified 30 Aug 2011
 By Tom Igoe

 http://www.arduino.cc/en/Tutorial/Fading

 This example code is in the public domain.

 */
#include <ESP8266WiFi.h>


int m1Pin = D2; //9;    // LED connected to digital pin 9
int m2Pin = D1; //9;    // LED connected to digital pin 9

void setup() {
  Serial.begin ( 115200 );
  Serial.print ( "Started" );
  pinMode(m1Pin, OUTPUT);
  pinMode(m2Pin, OUTPUT);
  analogWrite(m1Pin, 128);
  analogWrite(m2Pin, 128);
  
  // nothing happens in setup
}

void loop() {
  // fade in from min to max in increments of 5 points:
  for (int fadeValue = 20 ; fadeValue <= 1020; fadeValue += 10) {
//    // sets the value (range from 0 to 255):
    analogWrite(m1Pin, fadeValue);
    analogWrite(m2Pin, fadeValue);
//    // wait for 30 milliseconds to see the dimming effect
    delay(30);
  }

//  // fade out from max to min in increments of 5 points:
  for (int fadeValue = 1020 ; fadeValue > 20; fadeValue -= 10) {
//    // sets the value (range from 0 to 255):
    analogWrite(m1Pin, fadeValue);
    analogWrite(m2Pin, fadeValue);
//    // wait for 30 milliseconds to see the dimming effect
    delay(30);
  }
}


