﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;

namespace udp
{
    public partial class Form1 : Form
    {
        
        public static IPAddress broadcast;
        public static IPEndPoint ep;
        public static string node_ip;
        public static string node_port;
        public static int port;
        public static Socket  s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        public static int left_speed;
        public static int right_speed;
        public static int both_speed;
        public static Boolean check_connect = false;
   
        public static Boolean both_check = false;
        public static int max_speed = 100;
       public Form1()
        {
           
            InitializeComponent();
            this.KeyPreview = true;
            progressBar1.Maximum = max_speed;
            progressBar1.Step = 1;
            progressBar1.ForeColor = Color.Red;
            progressBar2.Maximum = max_speed;
            progressBar2.Step = 1;
            progressBar2.ForeColor = Color.Blue;
            textBox1.Text = ("192.168.43.154");
            textBox2.Text = ("4210");
          

     
        
       
        }

        private void button2_Click(object sender, EventArgs e)
        {
            


            node_ip = textBox1.Text.ToString();
            node_port = textBox2.Text.ToString();
            port = Int32.Parse(node_port);
           
            
           
            broadcast = IPAddress.Parse(node_ip);
            ep = new IPEndPoint(broadcast, port);
            label10.Text = "Connected";
            check_connect = true;
            byte[] sendbuf = Encoding.ASCII.GetBytes("connected");
            s.SendTo(sendbuf, ep);

            textBox1.Enabled = false;
            textBox2.Enabled = false;
            button2.Enabled = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)

        {
            if (check_connect)
            {
                if (e.KeyCode == Keys.Up && (e.Control))
                {
                    if (both_check == true) { left_speed = right_speed = both_speed; both_check = false; }
                    
                    if (left_speed >= max_speed) left_speed = max_speed;
                    else { left_speed += 1; }
                    progressBar2.Value = left_speed;
                    label6.Text = left_speed.ToString();
                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;l;" + left_speed);
                    s.SendTo(sendbuf, ep);


                }
                if (e.KeyCode == Keys.Down && (e.Control))
                {
                    if (both_check == true) { left_speed = right_speed = both_speed; both_check = false; }
                    if (left_speed <= 0) left_speed = 0;
                    else { left_speed -= 1; }

                    progressBar2.Value = left_speed;
                    label6.Text = left_speed.ToString();

                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;l;" + left_speed);
                    s.SendTo(sendbuf, ep);


                }

                if (e.KeyCode == Keys.Up && !(e.Control) && !(e.Shift))
                {
                    if (both_check == true) { left_speed = right_speed = both_speed; both_check = false; }
                    if (right_speed >= max_speed) right_speed = max_speed;
                    else { right_speed += 1; }
                    progressBar1.Value = right_speed;
                    label4.Text = right_speed.ToString();

                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;r;" + right_speed);
                    s.SendTo(sendbuf, ep);


                }
                if (e.KeyCode == Keys.Down && !(e.Control) && !(e.Shift))
                {
                    if (both_check == true) { left_speed = right_speed = both_speed; both_check = false; }
                    if (right_speed <= 0) right_speed = 0;
                    else { right_speed -= 1; }
                    progressBar1.Value = right_speed;
                    label4.Text = right_speed.ToString();
                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;r;" + right_speed);
                    s.SendTo(sendbuf, ep);
                   

                }

                if (e.KeyCode == Keys.Space)
                {
                    right_speed = 0;
                    left_speed = 0;
                    progressBar1.Value = right_speed;
                    progressBar2.Value = left_speed;
                    label4.Text = "Stopped";
                    label6.Text = "Stopped";
                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;b;0");
                    s.SendTo(sendbuf, ep);



                }
                if (e.KeyCode == Keys.Up && (e.Shift) && !(e.Control))
                {
                    both_check = true;
                    if (both_speed >= max_speed) both_speed = max_speed;
                    else { both_speed += 1; }

                    progressBar2.Value = progressBar1.Value = both_speed;
                    
                    label6.Text = label4.Text = both_speed.ToString();
                    
                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;b;" + both_speed);
                    s.SendTo(sendbuf, ep);
                  


                }
                if (e.KeyCode == Keys.Down && (e.Shift) && !(e.Control))
                {
                    both_check = true;
                    if (both_speed <= 0) both_speed = 0;
                    else { both_speed -= 1; }

                    progressBar2.Value = progressBar1.Value = both_speed;

                    label6.Text = label4.Text = both_speed.ToString();

                    byte[] sendbuf = Encoding.ASCII.GetBytes("s;b;" + both_speed);
                    s.SendTo(sendbuf, ep);


                }

            }  

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

      
    }
}
