#define motorR_1 D2
#define motorR_2 D5

#define motorL_1 D8
#define motorL_2 D6

#include <PID_v1.h>

const byte pwmmode = 1;

const byte motorR_1_int = D3;
const byte motorR_2_int = D7;
const byte motorL_1_int = D1;
const byte motorL_2_int = D0;

volatile int CurVal_R = 0;
volatile int CurVal_L = 0;


double Setpoint_R, Input_R, Output_R;
double Setpoint_L, Input_L, Output_L;

double Kp_R = 10, Ki_R = 28, Kd_R = 1;
double Kp_L = 10, Ki_L = 28, Kd_L = 1;

PID PID_R(&Input_R, &Output_R, &Setpoint_R, Kp_R, Ki_R, Kd_R, DIRECT);
PID PID_L(&Input_L, &Output_L, &Setpoint_L, Kp_L, Ki_L, Kd_L, DIRECT);


long last_milli, lastVal_R, lastVal_L, CurSpeed_R, CurSpeed_L;

void setup() {

  CurSpeed_R = 0; //lastVal_R - CurVal_R;
  CurSpeed_L = 0; //LastVal_L - CurVal_R;
  last_milli = 0; lastVal_R = 0; lastVal_L = 0;


  Input_R = CurVal_R;
  Input_L = CurVal_L;
  if(pwmmode == 0){
  Setpoint_R = -1500;
  Setpoint_L = 1500;
  }
  else{
  Setpoint_R = -10;
  Setpoint_L = -20;
  }
  PID_R.SetMode(AUTOMATIC);
  PID_L.SetMode(AUTOMATIC);

  Serial.begin(9600);

  pinMode(motorR_1, OUTPUT);
  pinMode(motorR_2, OUTPUT);
  pinMode(motorL_1, OUTPUT);
  pinMode(motorL_2, OUTPUT);

  digitalWrite(motorR_1 , LOW);
  digitalWrite(motorR_2 , LOW);
  digitalWrite(motorL_1 , LOW);
  digitalWrite(motorL_2 , LOW);

  pinMode(motorR_1_int, INPUT_PULLUP);
  pinMode(motorR_2_int, INPUT_PULLUP);
  pinMode(motorL_1_int, INPUT_PULLUP);
  pinMode(motorL_2_int, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(motorR_1_int), handleInterrupt_R, CHANGE);
  attachInterrupt(digitalPinToInterrupt(motorL_1_int), handleInterrupt_L, CHANGE);

}
void handleInterrupt_R() {
  if (digitalRead(motorR_1_int) == digitalRead(motorR_2_int)) {
    CurVal_R++;
  } else {
    CurVal_R--;
  }
}

void handleInterrupt_L() {
  if (digitalRead(motorL_1_int) == digitalRead(motorL_2_int)) {
    CurVal_L++;
  } else {
    CurVal_L--;
  }
}




void SetPWM(int pwm_R , int pwm_L) {


  if (pwm_R > 0) {
    analogWrite(motorR_2, 0);
    analogWrite(motorR_1, pwm_R);     // foward_R
  }
  else {
    pwm_R = pwm_R * -1;
    analogWrite(motorR_1, 0);           // Backward_R
    analogWrite(motorR_2, pwm_R);
  }


  if (pwm_L > 0) {

    analogWrite(motorL_2, 0);
    analogWrite(motorL_1, pwm_L);     // foward_L

  }
  else
  {
    pwm_L = pwm_L * -1;
    analogWrite(motorL_1, 0);           // Backward_L
    analogWrite(motorL_2, pwm_L);
  }



}

void loop() {

  if (millis() - last_milli > 100) {
    last_milli = millis();
    CurSpeed_R = CurVal_R -lastVal_R  ;
    CurSpeed_L =  CurVal_L - lastVal_L ;
    lastVal_R = CurVal_R;
    lastVal_L = CurVal_L;
  }


  PID_L.SetOutputLimits(-1023, 1023);
  PID_R.SetOutputLimits(-1023, 1023);


  if(pwmmode == 0){
    Input_R = CurVal_R;
    Input_L = CurVal_L;
  } else{
    Input_R = CurSpeed_R;
    Input_L = CurSpeed_L;
  }

  PID_R.Compute();
  PID_L.Compute();

  SetPWM(Output_R, Output_L);

  if(pwmmode == 0){
    Serial.print("DMODE:");
    }
    else{
    Serial.print("SMODE:");
      
    }
  Serial.print(Input_R); Serial.print(":C:T:"); Serial.print(Setpoint_R);
  Serial.print(" ::OUT_R:");Serial.print(Output_R); 
  Serial.print(" :-: ");
  Serial.print(Input_L); Serial.print(":C:T:"); Serial.print(Setpoint_L);
  Serial.print(" ::OUT_L:");Serial.println(Output_L);
  
 
}




