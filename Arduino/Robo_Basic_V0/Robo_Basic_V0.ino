#define motorR_1 D2
#define motorR_2 D5

#define motorL_1 D8
#define motorL_2 D6
#define MaxPWM ((long)1023)


#define MOTORS 2
#define MOTORL 0
#define MOTORR 1


const byte motorR_1_int = D3;
const byte motorR_2_int = D7;
const byte motorL_1_int = D1;
const byte motorL_2_int = D0;

struct PIDVals {
  int  TargetVal;
  int  DGain;
  int  IGain;
  int  PGain;
  int  integral;
  int  pError;

  int   Mode;
  int  INTHIGH;
  int  INTLOW;
  int Error;
  int Eerror;
  int IBuf;
  int CurVal;
};

PIDVals PIDR, PIDL;


void setup() {
  Serial.begin(9600);
  
pinMode(motorR_1, OUTPUT);
pinMode(motorR_2, OUTPUT);


  pinMode(motorR_1_int, INPUT_PULLUP);
  pinMode(motorR_2_int, INPUT_PULLUP);
  pinMode(motorL_1_int, INPUT_PULLUP);
  pinMode(motorL_2_int, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(motorR_1_int), handleInterrupt_A, CHANGE);
  attachInterrupt(digitalPinToInterrupt(motorL_1_int), handleInterrupt_B, CHANGE);

  PIDR.DGain = 1;
  PIDR.IGain = 10;
  PIDR.PGain = 200;
  PIDR.integral = 0;
  PIDR.pError = 0;

  PIDR.Mode = 0;
  PIDR.INTHIGH = 3000;
  PIDR.INTLOW = -3000;
  PIDR.Error = 0;
  PIDR.Eerror = 0;
  PIDR.IBuf = 0;

  PIDL.DGain = 0;
  PIDL.IGain = 10;
  PIDL.PGain = 500;
  PIDL.integral = 0;
  PIDL.pError = 0;

  PIDL.Mode = 0;
  PIDL.INTHIGH = 3000;
  PIDL.INTLOW = -3000;
  PIDL.Error = 0;
  PIDL.Eerror = 0;
  PIDL.IBuf = 0;


  //testing
  PIDR.TargetVal = 1500;
  PIDL.TargetVal = 1500;


}

void handleInterrupt_A() {
  if (digitalRead(motorR_1_int) == digitalRead(motorR_2_int)) {
    PIDR.CurVal++;
  } else {
    PIDR.CurVal--;
  }
}

void handleInterrupt_B() {
  if (digitalRead(motorL_1_int) == digitalRead(motorL_2_int)) {
    PIDL.CurVal++;
  } else {
    PIDL.CurVal--;
  }
}


int CalcPID(PIDVals & pidvals) {

  int err, derr, cval;
  err = (pidvals.TargetVal - pidvals.CurVal) / 2; //curval
  pidvals.Error = err;
  pidvals.integral += err / 10;
  derr = err - pidvals.Eerror;
  pidvals.Eerror = err;
  if (pidvals.integral > pidvals.INTHIGH) pidvals.integral = pidvals.INTHIGH;
  if (pidvals.integral < pidvals.INTLOW) pidvals.integral = pidvals.INTLOW;
  pidvals.IBuf = err;
  pidvals.IBuf = pidvals.IBuf * ((long) pidvals.PGain);
  pidvals.IBuf = pidvals.IBuf + ((long) pidvals.integral) * ((long) pidvals.IGain);
  pidvals.IBuf = pidvals.IBuf + ((long) derr) * ((long) pidvals.DGain);
  pidvals.IBuf = pidvals.IBuf / 100;
  cval = 1;
  if (pidvals.IBuf < 0) {
    cval = -1;
    pidvals.IBuf = (-1 * pidvals.IBuf);
  }
  if (pidvals.IBuf > MaxPWM) pidvals.IBuf = MaxPWM;
  pidvals.IBuf = pidvals.IBuf * cval;

  return (int) pidvals.IBuf;
}


void SetPWM(int MOTOR, int pwm) {

  if (MOTOR == 1){
  if (pwm < 0) {
    
    pwm = -1 * pwm;
    digitalWrite(motorR_1, 0);
    analogWrite(motorR_2, pwm);
  }

  digitalWrite(motorR_2, 0);
  analogWrite(motorR_1, pwm);     // foward_R

}
}



void loop() {
  CalcPID(PIDR);
  SetPWM(MOTORR, PIDR.IBuf);
  CalcPID(PIDL);
  SetPWM(MOTORL, PIDL.IBuf);

  Serial.print(PIDR.CurVal);
  Serial.print(" err is ");
  Serial.print(PIDR.Error);
  Serial.print(" pwm is ");
  Serial.print(PIDR.IBuf);
  Serial.print("  :::: ");

  Serial.print(PIDL.CurVal);
  Serial.print(" err is ");
  Serial.print(PIDL.Error);
  Serial.print(" pwm is ");
  Serial.println(PIDL.IBuf);
}


/*   analogWrite(motorR_1, 0);
  digitalWrite(motorR_2 , LOW);
  delay(2000);*/

