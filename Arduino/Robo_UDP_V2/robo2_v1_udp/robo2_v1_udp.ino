#include <PID_v1.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

WiFiUDP Udp;
unsigned int localUdpPort = 4210;
char packet_in[255];
char  replyPacekt[] = "received";
String command;
String ip;
const char* ssid = "AndroidAP";
const char* password = "87654321";


#define motor_R D1
#define motor_L D2
#define DIR_R D4
#define DIR_L D5
#define motorL_encoder_A D3
#define motorL_encoder_B D6
#define motorR_encoder_A D7
#define motorR_encoder_B D8
#define FORWARD         LOW
#define BACKWARD        HIGH

long last_milli, lastVal_R, lastVal_L, CurSpeed_R, CurSpeed_L;
long last_dir_t;
char cur_dir = 0;

volatile int CurVal_R = 0;
volatile int CurVal_L = 0;
double Setpoint_R, Input_R, Output_R;
double Setpoint_L, Input_L, Output_L;
double Setpoint_R_Ultimate, Setpoint_L_Ultimate;
double Kp_R = 10, Ki_R = 0.01, Kd_R = 0;
double Kp_L = 10, Ki_L = 0.01, Kd_L = 0;
int max_pwm = 500;
int x=0,y=0;

PID PID_R(&Input_R, &Output_R, &Setpoint_R, Kp_R, Ki_R, Kd_R, DIRECT);
PID PID_L(&Input_L, &Output_L, &Setpoint_L, Kp_L, Ki_L, Kd_L, DIRECT);

void setup() {

  last_dir_t = 0;
  
  Serial.begin(9600);
  analogWriteFreq(500);

  Serial.println("started...");

  Setpoint_R = 0;
  Setpoint_L = 0;
  
  CurSpeed_R = 0;
  CurSpeed_L = 0;

  last_milli = 0; lastVal_R = 0; lastVal_L = 0;

  pinMode(motor_R , OUTPUT);
  pinMode(motor_L , OUTPUT);
  pinMode(DIR_R , OUTPUT);
  pinMode(DIR_L , OUTPUT);


  digitalWrite(motor_R , LOW);
  digitalWrite(motor_L , LOW);
  digitalWrite(DIR_R, LOW);
  digitalWrite(DIR_L, LOW);

  pinMode(motorL_encoder_A, INPUT);
  pinMode(motorL_encoder_B, INPUT);
  pinMode(motorR_encoder_A, INPUT);
  pinMode(motorR_encoder_B, INPUT);

  attachInterrupt(digitalPinToInterrupt(motorR_encoder_B), handleInterrupt_R, RISING);
  attachInterrupt(digitalPinToInterrupt(motorL_encoder_A), handleInterrupt_L, RISING);

  PID_R.SetMode(AUTOMATIC);
  PID_L.SetMode(AUTOMATIC);
  PID_R.SetOutputLimits(-1*max_pwm, max_pwm);
  PID_L.SetOutputLimits(-1*max_pwm, max_pwm);


  WiFi.begin(ssid, password);
  //Serial.print("Starting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("$");
  }
  //Serial.println(" connected");
    Udp.begin(localUdpPort);
  Serial.printf("Now listening at IP %s, UDP port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
  ip = WiFi.localIP().toString().c_str();

  delay(2000);
}

void handleInterrupt_R() {
  if (digitalRead(motorR_encoder_A) == digitalRead(motorR_encoder_B)) {
    CurVal_R++;
  } else {
    CurVal_R--;
  }
}

void handleInterrupt_L() {
  if (digitalRead(motorL_encoder_A) == digitalRead(motorL_encoder_B)) {
    CurVal_L++;
  } else {
    CurVal_L--;
  }
}

void SetPWM(int pwm_L , int pwm_R) {
  if (pwm_L < 0) pwm_L = 0;
  if (pwm_R < 0) pwm_R = 0;
  if (pwm_L > max_pwm) pwm_L = max_pwm;
  if (pwm_R > max_pwm) pwm_R = max_pwm;

  analogWrite(motor_R , pwm_R);
  analogWrite(motor_L , pwm_L);
}

void SetPoints(){
  int mag = sqrt((y*y)+(x*x));
  int ratio = x;
  int minor;
  if(ratio<0) ratio*=-1;
  //major = (ratio*mag)/100;
  minor = ((100-ratio)*mag)/100;
  if(x<0){
    Setpoint_R_Ultimate = mag; 
    Setpoint_L_Ultimate = minor;
  }
  if(x>0){
    Setpoint_R_Ultimate = minor;
    Setpoint_L_Ultimate = mag; 
  }
    if(x==0){
    Setpoint_R_Ultimate = mag;
    Setpoint_L_Ultimate = mag;
  }

  if(x<=0 && y<0){digitalWrite(DIR_R, FORWARD);digitalWrite(DIR_L, BACKWARD);}
  else if(x>0 && y<0){digitalWrite(DIR_R, BACKWARD);digitalWrite(DIR_L, FORWARD);}
  else {digitalWrite(DIR_R, FORWARD);digitalWrite(DIR_L, FORWARD);}
}


void getUDP() {
  int index;
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {
    // receive incoming UDP packets
  //Serial.printf("Received %d bytes from %s, port %d\n", packetSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(packet_in, 255);
    if (len > 0)
    {
      packet_in[len] = 0;
    }
    Serial.println(packet_in);
    command = packet_in;
    //Serial.println(command);

    //Mx;60;y;40
    //Cx;60;y;40 etc. should be sent on serial
  
    if(packet_in[0]!='M'){
      Serial.write(packet_in);
    }
    else{
      if (command.substring(1, 3).equals("x;")) {
        x = command.substring(3).toInt();
        //if(command.substring(2,1).equals("-")) x = -1*x;
        index = command.lastIndexOf(";");
        y = command.substring(index+1).toInt();
        //if(command.substring(index+1).equals("-")) y = -1*y;
        SetPoints();
//        Serial.print("x: ");Serial.print(x);Serial.print("; ");
//        Serial.print("y: ");Serial.print(y);Serial.println("; ");
//        Serial.print("SetPoint_L:"); Serial.print(Setpoint_L); Serial.print("; SetPoint_R:"); Serial.println(Setpoint_R);
      }
    }
    
//      if (command.substring(2,3).equals("l")) {
//        String val_s = command.substring(4);
//        int val = val_s.toInt();
//        Setpoint_L = val;
//        Serial.print("L ");
//        Serial.println(Setpoint_L);
//      }
//
//    
//    
//    if (command.substring(0, 2).equals("s;")) {
//      if (command.substring(2,3).equals("l")) {
//        String val_s = command.substring(4);
//        int val = val_s.toInt();
//        Setpoint_L = val;
//        Serial.print("L ");
//        Serial.println(Setpoint_L);
//      }
//      if (command.substring(2,3).equals("r")) {
//        String val_s = command.substring(4);
//        int val = val_s.toInt();
//        Setpoint_R = val;
//        Serial.print("R ");
//        Serial.println(Setpoint_R);
//      }
//      if (command.substring(2,3).equals("b")) {
//        String val_s = command.substring(4);
//        int val = val_s.toInt();
//        Setpoint_R = val;
//        Setpoint_L = val;
//        Serial.print("B ");
//        Serial.println(Setpoint_R);
//      }
//    }
  }
}

void loop() {
  if (millis() - last_milli > 10) {
    last_milli = millis();
    CurSpeed_R = CurVal_R - lastVal_R  ;
    CurSpeed_L =  CurVal_L - lastVal_L ;
    lastVal_R = CurVal_R;
    lastVal_L = CurVal_L;
  }
  
//  if (millis() - last_dir_t > 5000) {
//    last_dir_t = millis();
//    
//    if(cur_dir == 0){   
//      digitalWrite(DIR_R, HIGH);
//      digitalWrite(DIR_L, HIGH);
//      cur_dir = 1;
//    }
//    else {  
//      digitalWrite(DIR_R, LOW);
//      digitalWrite(DIR_L, LOW);
//      cur_dir = 0;
//    }
//  }

  Input_R = CurSpeed_R*10;
  Input_L = CurSpeed_L*10;
  
  if(Setpoint_R<Setpoint_R_Ultimate) Setpoint_R+=0.05;
  if(Setpoint_R>Setpoint_R_Ultimate) Setpoint_R-=0.05;
  if(Setpoint_L<Setpoint_L_Ultimate) Setpoint_L+=0.05;
  if(Setpoint_L>Setpoint_L_Ultimate) Setpoint_L-=0.05;
  
  PID_R.Compute();
  PID_L.Compute();

  SetPWM(Output_L, Output_R);
  getUDP();
  Serial.flush();
  
//  Serial.print("CurVal_R: "); Serial.print(CurVal_R);
//  Serial.print("  ");
//  Serial.print("CurSpeed_R: "); Serial.print(CurSpeed_R);
//  Serial.print("  ");
//Serial.print("PID_R: "); Serial.print(Output_R);
// Serial.print("  ");
//Serial.print("SET_R: "); Serial.print(Setpoint_R);
// Serial.print(":::");
//
//  Serial.print("CurSpeed_L: "); Serial.print(CurSpeed_L);
//  Serial.print("  ");
////Serial.print("CurVal_L: "); Serial.print(CurVal_L);
////Serial.print("  ");
//Serial.print("PID_L: "); Serial.print(Output_L);
// Serial.print("  ");
//Serial.print("SER_L: "); Serial.print(Setpoint_L);
// Serial.println("  ");

}

