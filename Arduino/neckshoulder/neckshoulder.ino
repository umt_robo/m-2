//L293Djkl

#include<Servo.h>
#define SH_SPEED      250
#define NECK_SPEED    250
#define GRABBER_INC   3
#define GRABBER_DEC   -3

int x,y; 
int Gr_Angle = 90;
int len;
char buffer[100];

//neck motor
const int NECK1 = 4;
const int NECK2 = 5;

//Servo scene paat
Servo grabber;

//Motor LEFT SHOULDER A
const int LS1 = 9; //motorPin1  = 9;  // Pin 14 of L293
const int LS2 = 10; //motorPin2  = 10;  // Pin 10 of L293
//Motor RIGHT SHOULDER 
const int RS1 = 11; //motorPin3  = 11; // Pin  7 of L293
const int RS2 = 12; //motorPin4  = 12;  // Pin  2 of L293


//This will run only one time.
void setup(){  
  Serial.begin(9600);
  Serial.println("HELLO BABA");
  
  //Set pins as outputs
  pinMode(LS1, OUTPUT);
  pinMode(LS2, OUTPUT);
  pinMode(RS1, OUTPUT);
  pinMode(RS2, OUTPUT);
  pinMode(NECK1, OUTPUT);
  pinMode(NECK2, OUTPUT);

  //Servo PIN 
  grabber.attach(13);

  analogWrite(LS1, 0);
  analogWrite(LS2, 0);
  analogWrite(RS1, 0);
  analogWrite(RS2, 0);
}

void loop(){
  int index;
  int i;
  analogWrite(NECK1, 0);
  analogWrite(NECK2, 0);
  analogWrite(RS1, 0);
  analogWrite(RS2, 0);
  analogWrite(LS1, 0);
  analogWrite(LS2, 0);

  
  //Serial.print(".");
    len = Serial.available(); if(len>100) len=100;
  for(i=0;i<100;i++){
    while(Serial.available()==0);
    buffer[i] = Serial.read();
    if(buffer[i]=='\n'){
      buffer[i]=0;
      len = i;
      break;
    }
  }
  if(i==100) return;
  String command(buffer);
  Serial.println(command);

  
  if( buffer[0]=='C' || buffer[0]=='R' || buffer[0]=='L' || buffer[0] == 'G' || buffer[0] == 'H'){
    if (command.substring(1,3).equals("x;")) {
      x = command.substring(3).toInt();
      if((index = command.lastIndexOf(";"))!=-1){
        y = command.substring(index+1).toInt();
        if(x>=-100 && x<=100 && y>=-100 && y<=100){
          switch(buffer[0]){
            case('C'):
              if(y>0){
                analogWrite(NECK2,0);
                analogWrite(NECK1,(float)y*2.5);
              } else if(y<0){
                analogWrite(NECK1,0);
                analogWrite(NECK2,(float)y*-2.5);
              } else{
                analogWrite(NECK1,0);
                analogWrite(NECK2,0);
              }
            break;
            case('R'):
              if(y>0){
                analogWrite(RS2,0);
                analogWrite(RS1,(float)y*2.5);
              } else if(y<0){
                analogWrite(RS1,0);
                analogWrite(RS2,(float)y*-2.5);
              } else{
                analogWrite(RS1,0);
                analogWrite(RS2,0);
              }
            break;
            case('L'):
              if(y>0){
                analogWrite(LS2,0);
                analogWrite(LS1,(float)y*2.5);
              } else if(y<0){
                analogWrite(LS1,0);
                analogWrite(LS2,(float)y*-2.5);
              } else{
                analogWrite(LS1,0);
                analogWrite(LS2,0);
              }
            break;
            case('G'):
            break;
            case('H'):
            break;
          }
        }
      }
    }
  }
  delay(200);
}
